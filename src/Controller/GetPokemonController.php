<?php

namespace App\Controller;

use App\Entity\Pokemon;
use App\Entity\Type;
use App\Repository\PokemonRepository;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;


class GetPokemonController extends AbstractController
{
    /**
     * @Route("/", name="list_pokemon")
     */
    public function index(PokemonRepository $pokemonRepository): Response
    {
        return $this->render('pokemon.html.twig', [
            'pokemon' => $pokemonRepository->findAll(),
        ]);
    }

    /**
    * @Route("/{type}", name="getByType")
    */
    public function getPokemonByType($type, TypeRepository $typeRepository) : Response
    {
        
        $type = $typeRepository->findOneByType(['type' => $type]);
        $jsonContent = $serializer->serialize($type, 'json');
        if(!$type){
            throw new NotFoundHttpException('Pas de Pokémon trouvé');
        }
    }
}
